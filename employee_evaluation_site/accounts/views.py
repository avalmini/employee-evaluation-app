"""
Copyright 2022 Maciej Płonowski, Michał Parchanowicz
This file is part of Employee evaluation app.
Employee evaluation app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Employee evaluation app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Employee evaluation app. If not, see <https://www.gnu.org/licenses/>.
"""

from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib import messages
from .forms import CreateUserForm


def register(request):
    if request.user.is_authenticated:
        return redirect("/")
    form = CreateUserForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            form.save()
            messages.success(request, "Zarejestrowano pomyślnie" )
            return redirect('login')
        else:
            messages.error(request, "Rejestracja nie powiodła się")
            errorsList = list(form.errors.items())
            for errors in errorsList:
                if errors[0] == 'username':
                    if "A user with that username already exists." in errors[1]:
                        messages.info(request, "Login jest już zajęty")
                    else:
                        messages.info(request, "Login jest niepoprawny")
                if errors[0] == 'email':
                    if "User with this Email already exists." in errors[1]:
                        messages.info(request, "Email jest już zajęty")
                    else:
                        messages.info(request, "Email jest niepoprawny")
                if errors[0] == 'password1':
                    if "This field is required." in errors[1]:
                        messages.info(request, "Hasło nieprawidłowe")
                if errors[0] == 'password2':
                    if "The two password fields didn’t match." in errors[1]:
                        messages.info(request, "Hasła muszą być takie same")
                    if "This password is too common." in errors[1]:
                        messages.info(request, "Hasło jest zbyt proste")
                    if "This password is entirely numeric." in errors[1]:
                        messages.info(request, "Hasło nie może składać się z samych cyfr")
                    if "This field is required." in errors[1] or "This password is too short. It must contain at least 8 characters." in errors[1]:
                        messages.info(request, "Hasło musi się składać z co najmniej 8 znaków")

    context = {'form': form}
    return render(request, 'accounts/register.html', context)


def login(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect('/')
        else:
            messages.error(request, "Login lub hasło są niepoprawne")

    context = {}
    return render(request, 'accounts/login.html', context)


def logout(request):
    auth_logout(request)
    return redirect('/')


