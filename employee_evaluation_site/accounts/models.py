"""
Copyright 2022 Maciej Płonowski, Michał Parchanowicz
This file is part of Employee evaluation app.
Employee evaluation app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Employee evaluation app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Employee evaluation app. If not, see <https://www.gnu.org/licenses/>.
"""

from django.db import models
from django.contrib.auth.models import AbstractUser


class Job(models.Model):
    name = models.CharField(
        max_length=250
    )

    class Meta:
        verbose_name_plural = 'Jobs'

    def __str__(self):
        return self.name


class Employee(models.Model):
    first_name = models.CharField(
        max_length=250,
    )
    last_name = models.CharField(
        max_length=250,
        blank=True,
    )
    academic_degree = models.CharField(
        max_length=250,
        blank=True,
    )
    job_title = models.ForeignKey(
        Job,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = 'Employees'

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class Staff(models.Model):
    first_name = models.CharField(
        max_length=250,
    )
    last_name = models.CharField(
        max_length=250,
        blank=True,
    )
    job_title = models.ForeignKey(
        Job,
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name_plural = 'Staff'

    def __str__(self):
        return '%s %s' % (self.first_name, self.last_name)


class User(AbstractUser):
    is_employee = models.BooleanField(
        default=False
    )
    email = models.EmailField(unique=True)
