"""
Copyright 2022 Maciej Płonowski, Michał Parchanowicz
This file is part of Employee evaluation app.
Employee evaluation app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Employee evaluation app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Employee evaluation app. If not, see <https://www.gnu.org/licenses/>.
"""

import nested_admin.nested
from django.contrib import admin
from django import forms
from django.forms import ChoiceField
from django_admin_listfilter_dropdown.filters import RelatedDropdownFilter, DropdownFilter, ChoiceDropdownFilter
from simple_history.admin import SimpleHistoryAdmin

from accounts.models import User
from evaluations.models import Evaluation, EvaluationInstance, EvaluationFormTemplate, FormSection, FormQuestion, \
    FormInput, Response


class FormInputInline(nested_admin.nested.NestedTabularInline):
    model = FormInput
    extra = 0


class FormQuestionInline(nested_admin.nested.NestedTabularInline):
    model = FormQuestion
    inlines = [FormInputInline]
    extra = 0


class FormSectionInline(nested_admin.nested.NestedTabularInline):
    model = FormSection
    inlines = [FormQuestionInline]
    extra = 0


class EvaluationFormTemplateInline(nested_admin.nested.NestedTabularInline):
    model = EvaluationFormTemplate
    inlines = [FormSectionInline]
    extra = 0


class EvaluationCustomForm(forms.ModelForm):
    DONT_CREATE = 'do_not_create'
    CREATE = 'create'
    CREATE_UNIQUE = 'create_unique'
    CHOICES = [(DONT_CREATE, 'Do not automatically create Evaluation Instances for users'),
               (CREATE, 'Automatically create Evaluation Instances for users'),
               (CREATE_UNIQUE, 'Automatically create unique Evaluation Instances for users')]
    create_instances = ChoiceField(choices=CHOICES)


def get_users_in_groups(groups):
    return User.objects.filter(groups__in=groups)


def create_all_instances(obj):
    objects = []
    for user in get_users_in_groups(obj.evaluated_group.all()):
        objects.append(EvaluationInstance(employee=user, evaluation=obj, evaluation_instance_state='New',
                                          message=''))
    EvaluationInstance.objects.bulk_create(objects)


def create_unique_instances(obj):
    objects = []
    employees_with_evaluation_instances = EvaluationInstance.objects.filter(evaluation=obj).values_list("employee_id",
                                                                                                        flat=True).distinct()
    users = get_users_in_groups(obj.evaluated_group.all()).exclude(id__in=employees_with_evaluation_instances)
    for user in users:
        objects.append(EvaluationInstance(employee=user, evaluation=obj, evaluation_instance_state='New',
                                          message=''))
    EvaluationInstance.objects.bulk_create(objects)


@admin.register(Evaluation)
class EvaluationAdmin(nested_admin.nested.NestedModelAdmin):
    list_display = ("id", "name", "get_groups", "evaluation_beginning", "evaluation_end")
    list_filter = (
        ("evaluated_group", RelatedDropdownFilter),
        ("evaluation_beginning", DropdownFilter),
        ("evaluation_end", DropdownFilter)
    )
    search_fields = ("name", "id", "evaluated_group__name")
    inlines = [EvaluationFormTemplateInline]
    form = EvaluationCustomForm

    def get_groups(self, obj):
        return ", ".join([g.name for g in obj.evaluated_group.all()])

    def save_model(self, request, obj, form, change):
        super(EvaluationAdmin, self).save_model(request, obj, form, change)
        create_instances = request.POST.get('create_instances')

        if create_instances == 'create':
            create_all_instances(obj)
        elif create_instances == 'create_unique':
            create_unique_instances(obj)

    get_groups.short_description = "Groups"


@admin.register(EvaluationInstance)
class EvaluationInstanceAdmin(nested_admin.nested.NestedModelAdmin):
    list_display = ("id", "employee", "evaluation", "evaluation_instance_state")
    list_filter = (
        ("employee", RelatedDropdownFilter),
        ("evaluation", RelatedDropdownFilter),
        ("evaluation_instance_state", ChoiceDropdownFilter)
    )
    search_fields = ("id", "employee__username", "evaluation__name",)


@admin.register(EvaluationFormTemplate)
class EvaluationFormTemplateAdmin(nested_admin.nested.NestedModelAdmin):
    list_display = ("id", "name", "description", "evaluation",)
    list_filter = (
        ("name", DropdownFilter),
        ("evaluation", RelatedDropdownFilter)
    )
    search_fields = ("id", "name", "evaluation__name",)
    inlines = [FormSectionInline]
    save_as = True


@admin.register(FormSection)
class FormSectionAdmin(nested_admin.nested.NestedModelAdmin):
    list_display = ("id", "name", "form_template", "description",)
    list_filter = (
        ("name", DropdownFilter),
        ("form_template", RelatedDropdownFilter)
    )
    search_fields = ("id", "name", "form_template__name",)
    inlines = [FormQuestionInline]


@admin.register(FormQuestion)
class FormQuestionAdmin(nested_admin.nested.NestedModelAdmin):
    list_display = ("id", "name", "form_section", "description",)
    list_filter = (
        ("name", DropdownFilter),
        ("form_section", RelatedDropdownFilter)
    )
    search_fields = ("id", "name", "form_section__name",)
    inlines = [FormInputInline]


@admin.register(FormInput)
class FormInputAdmin(nested_admin.nested.NestedModelAdmin):
    list_display = ("id", "name", "form_question",)
    list_filter = (
        ("name", DropdownFilter),
        ("form_question", RelatedDropdownFilter)
    )
    search_fields = ("id", "name", "form_question__name",)


class ResponseHistoryAdmin(SimpleHistoryAdmin):
    list_display = ("id", "evaluation_instance", "form_input", "response_state")
    list_filter = (
        ("evaluation_instance", RelatedDropdownFilter),
        ("form_input", RelatedDropdownFilter),
        ("response_state", ChoiceDropdownFilter)
    )
    search_fields = ("id",
                     "evaluation_instance__employee__username",
                     "evaluation_instance__evaluation__name",
                     "form_input__name",
                     "response_state"
                     )


admin.site.register(Response, ResponseHistoryAdmin)
