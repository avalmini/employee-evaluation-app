"""
Copyright 2022 Maciej Płonowski, Michał Parchanowicz
This file is part of Employee evaluation app.
Employee evaluation app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Employee evaluation app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Employee evaluation app. If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.exceptions import FieldError
from django.core.paginator import Paginator
from django.http import Http404
from django.shortcuts import render, redirect

from evaluations.models import EvaluationInstance, EvaluationFormTemplate, FormSection, FormQuestion, \
    FormInput, Response, Evaluation


def createNewResponses(request, evaluation_id, evaluationInstance, formInputs):
    """
    :param formInputs:
    :param evaluationInstance:
    :param request:
    :param evaluation_id:
    Creates new Response objects if any new responses are created in the form
    """
    requestKeys = dict.keys(request.POST)
    for requestKey in requestKeys:
        if requestKey.startswith('newResponse', 0, len(requestKey)):
            inputId = requestKey[11:]
            for index, value in enumerate(request.POST.getlist('newResponse' + inputId)):
                if value != '':
                    responsePoints = request.POST.getlist('newStringPoints' + str(inputId))[index]
                    if responsePoints == '':
                        responsePoints = None
                    Response.objects.create(evaluation_instance=evaluationInstance,
                                            form_input=formInputs.filter(id=inputId).first(),
                                            response=value,
                                            points=responsePoints,
                                            response_state='New')


@login_required
def fillEvaluationTemplate(request, evaluation_id):
    """This function displays the user evaluation form.
    In case of POST type request, the function checks the inputs in the form one by one.
    In case of confirmation with the 'save' button:
        If there is a response to the input, this response is updated, if not a new one is created.
        If the input data type is not boolean and had a response after sending an empty string, the response is deleted.
        The created responses have the status: 'New'
    In case of confirmation with the 'send' button:
        If there is a response to the input, this response is updated, if not a new one is created.
        If the response is not answered, a response is created with an empty string.
        When the responses are assessed, responses have the status: 'Scored', if not then have the status: 'Completed'."""
    try:
        evaluationInst = EvaluationInstance.objects.get(pk=evaluation_id)
    except EvaluationInstance.DoesNotExist:
        raise Http404("Formularz nie istnieje")
    if evaluationInst.employee == request.user:
        evaluationFormTemplates = EvaluationFormTemplate.objects.filter(evaluation=evaluationInst.evaluation)
        formSections = FormSection.objects.filter(form_template__in=evaluationFormTemplates)
        formQuestions = FormQuestion.objects.filter(form_section__in=formSections)
        formInputs = FormInput.objects.filter(form_question__in=formQuestions)
        responses = Response.objects.filter(evaluation_instance=evaluationInst, form_input__in=formInputs)
        context = {'evaluationInst': evaluationInst, 'evaluationFormTemplates': evaluationFormTemplates,
                   'formSections': formSections, 'formQuestions': formQuestions, 'formInputs': formInputs,
                   'responses': responses}

        if request.method == 'POST':
            if 'save' in request.POST:
                for input in formInputs:
                    if input.type == 'ST':
                        for response in responses:
                            if response.form_input == input:
                                try:
                                    # deletes empty responses
                                    if request.POST["stringResponse" + str(response.id)] == '':
                                        Response.objects.filter(id=response.id).delete()
                                    else:
                                        resp = request.POST["stringResponse" + str(response.id)]
                                        points = request.POST["stringPoints" + str(response.id)]
                                        if points == '':
                                            points = None
                                        Response.objects.filter(id=response.id).update(response=resp, points=points)
                                except KeyError:
                                    # deletes responses not passed in the request body
                                    Response.objects.filter(id=response.id).delete()
                    else:
                        try:
                            resp = Response.objects.get(evaluation_instance=evaluationInst, form_input=input)
                        except Response.DoesNotExist:
                            resp = None
                    if input.type == 'BO':
                        try:
                            response = request.POST["response" + str(input.id)]
                            points = request.POST["points" + str(input.id)]
                            if points == '':
                                points = None
                            if resp is not None:
                                Response.objects.filter(id=resp.id).update(response=response, points=points)
                            else:
                                Response.objects.create(evaluation_instance=evaluationInst, form_input=input,
                                                        response=response, points=points, response_state='New')
                        except KeyError:
                            ""
                    else:
                        try:
                            if request.POST["response" + str(input.id)] == '':
                                if resp is not None:
                                    Response.objects.filter(id=resp.id).delete()
                            else:
                                response = request.POST["response" + str(input.id)]
                                points = request.POST["points" + str(input.id)]
                                if points == '':
                                    points = None
                                if resp is not None:
                                    Response.objects.filter(id=resp.id).update(response=response, points=points)
                                else:
                                    Response.objects.create(evaluation_instance=evaluationInst, form_input=input,
                                                            response=response, points=points, response_state='New')
                        except KeyError:
                            ""

            if 'send' in request.POST:
                for input in formInputs:
                    try:
                        if input.type == 'ST':
                            for response in responses:
                                if response.form_input == input:
                                    try:
                                        # deletes empty responses
                                        if request.POST["stringResponse" + str(response.id)] == '':
                                            Response.objects.filter(id=response.id).delete()
                                        else:
                                            resp = request.POST["stringResponse" + str(response.id)]
                                            points = request.POST["stringPoints" + str(response.id)]
                                            response_state = 'Completed'
                                            if points == '':
                                                points = None
                                            Response.objects.filter(id=response.id).update(
                                                response=resp,
                                                points=points,
                                                response_state=response_state)
                                    except KeyError:
                                        # deletes responses not passed in the request body
                                        Response.objects.filter(id=response.id).delete()
                            EvaluationInstance.objects.filter(id=evaluation_id).update(evaluation_instance_state='Sent')
                        else:
                            try:
                                resp = Response.objects.get(evaluation_instance=evaluationInst, form_input=input)
                            except Response.DoesNotExist:
                                resp = None
                            try:
                                response = request.POST["response" + str(input.id)]
                            except KeyError:
                                response = ''
                            points = request.POST["points" + str(input.id)]
                            response_state = 'Completed'
                            if points == '':
                                points = None
                            if resp is not None:
                                Response.objects.filter(id=resp.id).update(response=response, points=points,
                                                                           response_state=response_state)
                            else:
                                Response.objects.create(evaluation_instance=evaluationInst, form_input=input,
                                                        response=response,
                                                        points=points, response_state=response_state)
                            EvaluationInstance.objects.filter(id=evaluation_id).update(evaluation_instance_state='Sent')
                    except KeyError:
                        ""
            createNewResponses(request, evaluation_id, evaluationInst, formInputs)
            return redirect('evaluationsList')
        return render(request, 'evaluations/evaluationInstance.html', context)
    else:
        return redirect("/")


@login_required
def scoreEvaluation(request, evaluation_id):
    """This function displays the evaluation scoring form.
    In case of GET type request, the function checks if there are any sections that are assigned to the users' group.
        If there are any, the scoring form is opened.
        If there aren't any, user is redirected to the list with an adequate error message.
    In case of POST type request, the function checks the inputs in the form one by one.
    In case of confirmation with the 'save' button:
        If there is a response to the input, response points are updated.
        Otherwise, a new one is created with status 'Completed'.
    In case of confirmation with the 'send' button:
        If there is a response to the input, response points are updated.
        Otherwise, a new one is created with status 'Scored'."""
    try:
        evaluation_instance = EvaluationInstance.objects.get(pk=evaluation_id)
    except EvaluationInstance.DoesNotExist:
        messages.error(request, 'Brak sekcji formularza przypisanych do Ciebie, lub formularz nie istnieje!')
        return scoringList(request)

    user_groups = request.user.groups.all()
    evaluation_form_templates = EvaluationFormTemplate.objects.filter(evaluation=evaluation_instance.evaluation)
    form_sections = FormSection.objects.filter(form_template__in=evaluation_form_templates,
                                               section_point_completer__in=user_groups)

    if form_sections.exists():
        form_questions = FormQuestion.objects.filter(form_section__in=form_sections)
        form_inputs = FormInput.objects.filter(form_question__in=form_questions)
        responses = Response.objects.filter(evaluation_instance=evaluation_instance, form_input__in=form_inputs)
        context = {'evaluationInst': evaluation_instance, 'evaluationFormTemplates': evaluation_form_templates,
                   'formSections': form_sections, 'formQuestions': form_questions, 'formInputs': form_inputs,
                   'responses': responses}

        if request.method == 'POST':
            if 'send' in request.POST:
                response_state = 'Scored'
            else:
                response_state = 'Completed'

            for form_input in form_inputs:
                for response in responses:
                    if response.form_input == form_input:
                        try:
                            if form_input.type == 'ST':
                                points = request.POST['stringPoints' + str(response.id)]
                            else:
                                points = request.POST['points' + str(form_input.id)]

                            response_object = Response.objects.filter(id=response.id).first()

                            if points == '':
                                response_object.points = 0
                            else:
                                response_object.points = points

                            response_object.response_state = response_state
                            response_object.save()
                        except KeyError:
                            ""

            if 'send' in request.POST:
                # check if all responses have 'Scored' status
                EvaluationInstance.objects.filter(id=evaluation_id).update(evaluation_instance_state='Scored')
            return redirect('scoringList')
        return render(request, 'evaluations/evaluationScoring.html', context)

    else:
        messages.error(request, 'Brak sekcji formularza przypisanych do Ciebie, lub formularz nie istnieje!')
        return scoringList(request)


def acceptEvaluation(request, evaluation_id):
    try:
        evaluation_instance = EvaluationInstance.objects.get(pk=evaluation_id)
    except EvaluationInstance.DoesNotExist:
        messages.error(request, 'Brak sekcji formularza przypisanych do Ciebie, lub formularz nie istnieje!')
        return acceptingList(request)

    user_groups = request.user.groups.all()
    evaluation_form_templates = EvaluationFormTemplate.objects.filter(evaluation=evaluation_instance.evaluation)
    form_sections = FormSection.objects.filter(form_template__in=evaluation_form_templates,
                                               section_point_completer__in=user_groups)

    if form_sections.exists():
        form_questions = FormQuestion.objects.filter(form_section__in=form_sections)
        form_inputs = FormInput.objects.filter(form_question__in=form_questions)
        responses = Response.objects.filter(evaluation_instance=evaluation_instance, form_input__in=form_inputs)
        context = {'evaluationInst': evaluation_instance, 'evaluationFormTemplates': evaluation_form_templates,
                   'formSections': form_sections, 'formQuestions': form_questions, 'formInputs': form_inputs,
                   'responses': responses}

        if request.method == 'POST':
            try:
                message = request.POST['message']
            except KeyError:
                message = None
            if 'accept' in request.POST:
                EvaluationInstance.objects.filter(id=evaluation_id).update(evaluation_instance_state='Accepted',
                                                                           message=message)
            else:
                EvaluationInstance.objects.filter(id=evaluation_id).update(evaluation_instance_state='Rejected',
                                                                           message=message)

            return redirect('acceptingList')
        return render(request, 'evaluations/evaluationAccepting.html', context)

    else:
        messages.error(request, 'Brak sekcji formularza przypisanych do Ciebie, lub formularz nie istnieje!')
        return scoringList(request)


@login_required
def evaluationsList(request):
    evaluation_instances = EvaluationInstance.objects.filter(employee__exact=request.user,
                                                             evaluation_instance_state='New')

    # Filtering and sorting
    evaluation_instances = queryset_filter(request, evaluation_instances)
    # Pagination
    page_obj = pagination(request, evaluation_instances)

    context = {'evaluationInstances': page_obj}
    return render(request, 'lists/evaluationsList.html', context)


@login_required
def scoringList(request):
    """
    This function displays sent evaluation instances.
    The only instances the user receives on the list are:
        Instances that contain sections with a group in section_point_completer that the user belongs to,
        Instances that have evaluation_instance_state = Sent
    """
    user_groups = request.user.groups.all()
    sections = FormSection.objects.filter(section_point_completer__in=user_groups).values('form_template')
    evaluation_templates = EvaluationFormTemplate.objects.filter(id__in=sections).values('evaluation')
    evaluations = Evaluation.objects.filter(id__in=evaluation_templates)
    evaluation_instances = EvaluationInstance.objects.filter(evaluation_instance_state__in=['Sent', 'Rejected']).filter(
        evaluation__in=evaluations)

    # Filtering and sorting
    evaluation_instances = queryset_filter(request, evaluation_instances)
    # Pagination
    page_obj = pagination(request, evaluation_instances)

    context = {'evaluationInstances': page_obj}
    return render(request, 'lists/scoringList.html', context)


@login_required
def acceptingList(request):
    """
    This function displays scored evaluation instances.
    The only instances the user receives on the list are:
        Instances that contain sections with a group in section_acceptor that the user belongs to,
        Instances that have evaluation_instance_state = Scored
    """

    if request.method == 'POST':
        evaluation_ids = request.POST.getlist('evaluations')
        evaluation_state = request.POST.get('action')
        for evaluation_id in evaluation_ids:
            EvaluationInstance.objects.filter(pk=evaluation_id).update(evaluation_instance_state=evaluation_state)

    user_groups = request.user.groups.all()
    sections = FormSection.objects.filter(section_acceptor__in=user_groups).values('form_template')
    evaluation_templates = EvaluationFormTemplate.objects.filter(id__in=sections).values('evaluation')
    evaluations = Evaluation.objects.filter(id__in=evaluation_templates)
    evaluation_instances = EvaluationInstance.objects.filter(evaluation_instance_state='Scored').filter(
        evaluation__in=evaluations)

    # Filtering and sorting
    evaluation_instances = queryset_filter(request, evaluation_instances)
    # Pagination
    page_obj = pagination(request, evaluation_instances)

    context = {'evaluationInstances': page_obj}
    return render(request, 'lists/acceptingList.html', context)


def queryset_filter(request, queryset):
    order = request.GET.get('order_by', '')
    if order is not None:
        try:
            return queryset.order_by(order)
        except FieldError:
            return queryset


def pagination(request, queryset):
    per_page = request.GET.get('per_page', 10)
    paginator = Paginator(queryset, per_page)  # Show x per page.
    page_number = request.GET.get('page')
    return paginator.get_page(page_number)