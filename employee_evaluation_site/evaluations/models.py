"""
Copyright 2022 Maciej Płonowski, Michał Parchanowicz
This file is part of Employee evaluation app.
Employee evaluation app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Employee evaluation app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Employee evaluation app. If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib.auth.models import Group
from django.core.validators import MinValueValidator
from django.db import models
from simple_history.models import HistoricalRecords

from employee_evaluation import settings


class Evaluation(models.Model):
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False,
    )
    evaluated_group = models.ManyToManyField(
        Group,
        default=None,
        blank=True,
        null=True,
    )
    evaluation_beginning = models.DateField()
    evaluation_end = models.DateField()

    class Meta:
        verbose_name_plural = 'Evaluations'

    def __str__(self):
        return '%s' % (self.name,)


class EvaluationInstance(models.Model):
    employee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="employee",
        blank=False,
    )
    evaluation = models.ForeignKey(
        Evaluation,
        on_delete=models.CASCADE,
    )
    NEW = 'New'
    SENT = 'Sent'
    SCORED = 'Scored'
    ACCEPTED = 'Accepted'
    REJECTED = 'Rejected'
    STATE_CHOICES = [
        (NEW, 'New'),
        (SENT, 'Sent'),
        (SCORED, 'Scored'),
        (ACCEPTED, 'Accepted'),
        (REJECTED, 'Rejected'),
    ]
    evaluation_instance_state = models.CharField(
        default='New',
        choices=STATE_CHOICES,
        max_length=9,
    )
    message = models.CharField(
        max_length=255,
        blank=True,
        null=False,
    )

    class Meta:
        verbose_name_plural = 'Evaluation Instances'
        ordering = ['-id']

    def __str__(self):
        return '%s %s' % (self.employee, self.evaluation)


class EvaluationFormTemplate(models.Model):
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False,
    )
    description = models.CharField(
        max_length=500,
        null=True,
        blank=True,
    )
    evaluation = models.ForeignKey(
        Evaluation,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name_plural = 'Evaluation forms'

    def __str__(self):
        return self.name


class FormSection(models.Model):
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False
    )
    description = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    section_completer = models.ForeignKey(
        Group,
        related_name='section_completer',
        null=True,
        on_delete=models.CASCADE,
    )
    section_point_completer = models.ForeignKey(
        Group,
        related_name='section_point_completer',
        null=True,
        on_delete=models.CASCADE,
    )
    section_acceptor = models.ForeignKey(
        Group,
        related_name='section_acceptor',
        null=True,
        on_delete=models.CASCADE,
    )
    form_template = models.ForeignKey(
        EvaluationFormTemplate,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name_plural = 'Form sections'

    def __str__(self):
        return self.name


class FormQuestion(models.Model):
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False
    )
    description = models.CharField(
        max_length=500,
        null=True,
        blank=True
    )
    max_points = models.IntegerField(
        null=True,
    )
    form_section = models.ForeignKey(
        FormSection,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name_plural = 'Form Questions'

    def __str__(self):
        return self.name


class FormInput(models.Model):
    name = models.CharField(
        max_length=250,
        null=False,
        blank=False,
    )
    INTEGER = 'IN'
    STRING = 'ST'
    BOOLEAN = 'BO'
    TYPE_CHOICES = [
        (INTEGER, 'Integer'),
        (STRING, 'String'),
        (BOOLEAN, 'Boolean'),
    ]
    type = models.CharField(
        choices=TYPE_CHOICES,
        max_length=2,
    )
    form_question = models.ForeignKey(
        FormQuestion,
        on_delete=models.CASCADE,
    )

    class Meta:
        verbose_name_plural = 'Form Inputs'

    def __str__(self):
        return self.name


class Response(models.Model):
    evaluation_instance = models.ForeignKey(
        EvaluationInstance,
        on_delete=models.CASCADE,
    )
    form_input = models.ForeignKey(
        FormInput,
        on_delete=models.CASCADE,
    )
    response = models.CharField(
        max_length=500,
        null=False,
        blank=True,
    )
    NEW = 'New'
    COMPLETED = 'Completed'
    SCORED = 'Scored'
    ACCEPTED = 'Accepted'
    REJECTED = 'Rejected'
    STATE_CHOICES = [
        (NEW, 'New'),
        (COMPLETED, 'Completed'),
        (SCORED, 'Scored'),
        (ACCEPTED, 'Accepted'),
        (REJECTED, 'Rejected'),
    ]
    response_state = models.CharField(
        choices=STATE_CHOICES,
        max_length=9,
    )
    points = models.FloatField(
        blank=True,
        null=True,
        validators=[MinValueValidator(0.0, message="Wartość nie może być mniejsza od 0")]
    )
    history = HistoricalRecords()

    class Meta:
        verbose_name_plural = 'Responses'

    def __str__(self):
        return '%s %s' % (self.form_input, self.evaluation_instance)
