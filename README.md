Copyright 2022 Maciej Płonowski, Michał Parchanowicz
This file is part of Employee evaluation app.
Employee evaluation app is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Employee evaluation app is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Employee evaluation app. If not, see <https://www.gnu.org/licenses/>.

# Employee evaluation app

To install dependencies, run
pip install -r /path/to/requirements.txt

To launch the app, do

python manage.py makemigrations
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver

To access admin panel navigate to http://127.0.0.1:8000/admin/
